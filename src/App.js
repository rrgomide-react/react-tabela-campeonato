import React, { Component } from 'react';
import './App.css';
import Rodada from './components/Rodada';
import Classificacao from './components/Classificacao';

import { ClassificacaoService } from './services/ClassificacaoService';

class App extends Component {
  state = {
    times: []
  };

  render() {
    console.log(this.state.times);
    return (
      <div>
        <Rodada times={this.state.times} />
        <Classificacao />
      </div>
    );
  }

  componentDidMount() {
    const service = new ClassificacaoService();

    this.setState({
      times: service.times
    });
  }
}

export default App;
