import React, { Component } from 'react';

export default class Rodada extends Component {
  montarRodada() {
    const jogos = [];

    for (let i = 0; i < this.props.times.length; i += 2) {
      jogos.push({
        id: i,
        times: [this.props.times[i], this.props.times[i + 1]],
        placar: [0, 0]
      });
    }

    console.log(jogos);

    return jogos;
  }

  render() {
    const jogos = this.montarRodada();

    return (
      <div>
        <table border="1">
          <thead>
            <tr>
              <th>Rodada</th>
            </tr>
          </thead>
          <tbody>
            {jogos.map(jogo => {
              return (
                <tr key={jogo.id}>
                  <td>
                    {jogo.times[0].nome} x {jogo.times[1].nome}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
